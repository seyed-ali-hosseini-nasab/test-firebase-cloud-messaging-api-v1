import os
import asyncio
from typing import Any, List

from async_firebase import AsyncFirebaseClient
from async_firebase.messages import AndroidConfig, FcmPushMulticastResponse


def run_asyncio_method(function: callable) -> Any:
    if os.name == 'nt':
        from functools import wraps
        from asyncio.proactor_events import _ProactorBasePipeTransport

        def silence_event_loop_closed(func):
            @wraps(func)
            def wrapper(self, *args, **kwargs):
                try:
                    return func(self, *args, **kwargs)
                except RuntimeError as e:
                    if str(e) != 'Event loop is closed':
                        raise

            return wrapper

        _ProactorBasePipeTransport.__del__ = silence_event_loop_closed(_ProactorBasePipeTransport.__del__)
        policy = asyncio.WindowsSelectorEventLoopPolicy()
        asyncio.set_event_loop_policy(policy)
    return asyncio.run(function)


class FirebaseAndroidConfig(dict):
    def __init__(self, client: AsyncFirebaseClient) -> None:
        super(FirebaseAndroidConfig, self).__init__()
        self.client = client
        self._default()

    def _default(self) -> None:
        self['priority'] = "high"
        self['ttl'] = 2419200
        self['collapse_key'] = "push"

    def build_config(self) -> AndroidConfig:
        return self.client.build_android_config(**self)


class FirebaseNotification:
    def __init__(self, client: AsyncFirebaseClient, path_service_account_file: str) -> None:
        self.client = client
        self.creds_from_service_account_file(path_service_account_file)

    def creds_from_service_account_file(self, path_service_account_file: str):
        self.client.creds_from_service_account_file(path_service_account_file)

    async def _send(self, device_tokens: List[str], configs: dict) -> FcmPushMulticastResponse:
        return await self.client.push_multicast(device_tokens=device_tokens, **configs)

    def send(self, device_tokens: List[str], configs: dict) -> FcmPushMulticastResponse:
        return run_asyncio_method(self._send(device_tokens, configs))
