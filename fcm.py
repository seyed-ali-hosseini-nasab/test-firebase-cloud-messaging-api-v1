import firebase_admin
from firebase_admin import credentials, messaging
from typing import List


def initialize_app(path_service_account_file: str) -> None:
    cred = credentials.Certificate(path_service_account_file)
    try:
        firebase_admin.initialize_app(cred)
    except ValueError as value_error:
        if 'The default Firebase app already exists.' in str(value_error):
            pass


def android_config(title: str, body: str, data: dict = None) -> messaging.AndroidConfig:
    return messaging.AndroidConfig(
        notification=messaging.AndroidNotification(title=title, body=body, click_action="TOP_STORY_ACTIVITY"),
        data=data, priority='high',
        ttl=2419200, collapse_key='push')


def webpush_config(title: str, body: str, data: dict = None) -> messaging.WebpushConfig:
    return messaging.WebpushConfig(notification=messaging.WebpushNotification(title=title, body=body), data=data)


def send_multicast(tokens: List[str], android: messaging.AndroidConfig,
                   webpush: messaging.WebpushConfig) -> messaging.BatchResponse:
    message = messaging.MulticastMessage(tokens=tokens, android=android, webpush=webpush)
    return messaging.send_multicast(message)


class FirebaseProductPriceAlarm:
    @classmethod
    def __new__(cls, *args, **kwargs) -> object:
        if not hasattr(cls, 'instance'):
            cls.instance = super(FirebaseProductPriceAlarm, cls).__new__(cls)

        return cls.instance

    def __init__(self, path_service_account_file: str) -> None:
        initialize_app(path_service_account_file)

    @property
    def tokens(self) -> List[str]:
        tokens = [
            '',
        ]
        return tokens

    def notifier(self) -> messaging.BatchResponse:
        title = 'لپه باقلا (۸۰۰ گرم) بادبادک'
        body = 'کمترین قیمت 34,000 تومان'
        android_data = {'deeplink': 'superz://product/?slug=لپه-باقلا-۸۰۰-گرم-بادبادک'}
        webpush_data = {}

        response = send_multicast(self.tokens, android_config(title, body, android_data),
                                  webpush_config(title, body, webpush_data))
        return response
