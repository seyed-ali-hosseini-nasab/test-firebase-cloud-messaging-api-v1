import os

from async_firebase import AsyncFirebaseClient
from typing import List
from dotenv import load_dotenv
from fcmasync import FirebaseAndroidConfig, FirebaseNotification


def android_config() -> FirebaseAndroidConfig:
    firebase_android_config = FirebaseAndroidConfig(AsyncFirebaseClient())
    firebase_android_config['data'] = {
        "discount": "15%",
        "key_1": "value_1",
        "timestamp": "2021-02-24T12:00:15",
    }
    firebase_android_config['title'] = 'Store Changes'
    firebase_android_config['body'] = 'Recent store changes'

    return firebase_android_config


def get_tokens() -> List[str]:
    tokens = [
        '',
    ]
    return tokens


def main() -> None:
    load_dotenv()
    firebase_notification = FirebaseNotification(AsyncFirebaseClient(), os.getenv('GOOGLE_APPLICATION_CREDENTIALS'))

    configs = {
        'android': android_config().build_config(),
    }
    response = firebase_notification.send(get_tokens(), configs)
    for r in response.responses:
        print(f'is success: {r.success}, exception: {r.exception}')


if __name__ == '__main__':
    main()
