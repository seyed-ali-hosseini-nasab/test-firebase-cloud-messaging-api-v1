import os
from dotenv import load_dotenv
from fcm import FirebaseProductPriceAlarm


def main() -> None:
    load_dotenv()
    firebase_product_price_alarm = FirebaseProductPriceAlarm(os.getenv('GOOGLE_APPLICATION_CREDENTIALS'))
    response = firebase_product_price_alarm.notifier()

    for r in response.responses:
        print(f'is success: {r.success}, exception: {r.exception}')


if __name__ == '__main__':
    main()
